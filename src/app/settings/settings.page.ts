import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  settingsService: SettingsService;

  constructor(public settingServ: SettingsService) {
    this.settingsService = settingServ;
  }

  ngOnInit() {
  }

}
