import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  usbDebugging: boolean;

  constructor() {
    this.usbDebugging = false;
  }
}
