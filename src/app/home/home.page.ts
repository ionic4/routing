import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService} from '../settings.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  //usbDebugging: any;
  settingsService: any;

  constructor(public router: Router, public settingsServ: SettingsService) {
    this.settingsService = settingsServ;
  }

  ngOnInit() {
  }

  show_settings() {
    this.router.navigateByUrl('settings');
  }
}
